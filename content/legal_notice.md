---
title: "Legal Notice"
slug: "legal_notice"
---

Information required under § 5 Para. 1 of the German Telemedia Act:

Alexander Griesbaum  
email: alexander(at)griesbaum.dev 

Responsible for the content according to § 55 Abs. 2 RStV:

Alexander Griesbaum
