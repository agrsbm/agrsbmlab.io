---
title: "Simple Screen Locking"
date: 2018-01-03T14:25:21+02:00
slug: "simple-screen-locking"
tags:
- linux
---

Let's write a short linux tutorial :) I will use [Arch Linux](https://www.archlinux.org/).

You want to lock your (laptop) screen while having lunch or going to the toilet? You don't want to use the built-in stuff from your DE or bloated [*xscreensaver*](https://www.jwz.org/xscreensaver/)? Here you go.

We will use [*slock*](https://tools.suckless.org/slock/) (installed size: 40 KiB) for locking and [*xautolock*](https://www.ibiblio.org/pub/linux/X11/screensavers/) (installed size: 36 KiB) for some automation. On Arch Linux, both packages are available in the [community] repo, so just use pacman to get them to your machine (Both of them should also be available in [universe] repo on Ubuntu).

    # pacman -Syu slock xautolock

Let's take a look into the huge config file of slock.. wait.. there is none, it's ready to use right out of the box ;)
By typing

    $ slock

you can lock your screen now. Screen will go dark then. To log back in, enter your password - that simple. Now feel free to create a shortcut for the command or go on reading, we will create one later.

Now we use xautolock to automatically lock the screen after n minutes of inactivity, so we are save if we forgot to lock our screen by hand. Add the following line to your $HOME/.xinitrc. Be sure to add it above the line that starts your window manager. n will be 10 minutes:

    xautolock -time 10 -locker slock &

By doing that, your locking mechanism is fully configured (See *man xautolock* for further options). Finally, we create a shortcut to lock the screen. I will show an example with the [awesome wm](https://awesomewm.org/), you can of course copy the command whereever you want. The xautolock command is used because xautolock is already running, so we don't have to touch slock on our own:

    awful.key({ modkey, "Shift" }, "x", function () awful.util.spawn( "xautolock -locknow", false ) end),


That's it - simple and leightweight screen locking.
